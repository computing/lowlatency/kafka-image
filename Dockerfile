FROM docker.io/confluentinc/cp-kafka:7.7.1-1-ubi8

USER root

ENV JMX_VERSION=0.20.0

# add entrypoint
COPY --chown=appuser:appuser bin/entrypoint /usr/bin/entrypoint

# add configuration for the JMX exporter
COPY jmx-exporter-config.yml /etc/jmx-exporter/config.yml

# download JMX exporter
RUN wget -nc -P /usr/share/jmx-exporter \
	https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/${JMX_VERSION}/jmx_prometheus_javaagent-${JMX_VERSION}.jar

# define default Kafka variables to expose JMX metrics via exporter
ENV JMX_CONFIG /etc/jmx-exporter/config.yml
ENV KAFKA_JMX_PORT 9101
ENV JMX_EXPORTER_PORT 7101

USER appuser

ENTRYPOINT ["/usr/bin/entrypoint"]
CMD ["/etc/confluent/docker/run"]
