# kafka-image

This image is based off of the official Confluent Platform Kafka image
(cp-kafka) with Kafka alongside a JMX exporter, on port 7101 by default.

## Usage

```
$ docker run <options> containers.ligo.org/computing/lowlatency/kafka-image:latest
```

## Verification

Verify broker is running:

```
$ docker run -it --net=host --rm edenhill/kcat:1.7.1 -b localhost:9092 -L -J
```

## Configuration

Configuration is passed directly to the images via the `--env/-e` options with
rules specified in
https://docs.confluent.io/platform/current/installation/docker/config-reference.html.

The default JMX exporter port can be changed by setting `$JMX_EXPORTER_PORT`.
